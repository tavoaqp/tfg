library(snow)
args=commandArgs()
dataDim=as.numeric(args[10])
minSize=as.numeric(args[11])
maxSize=as.numeric(args[12])
sims=as.numeric(args[13])
filename<- paste("/home/ubuntu/params_dim",dataDim,"_min",minSize,"_max",maxSize,".txt",sep="")
cmest.function <- function(loopIdx,size) {
     print(size)
     mat<-mvrnorm(size,mu=numeric(dataDim),Sigma=diag(dataDim))
     mcdout<-DetMCD(mat)
     mcdset<-ifelse(c(1:size) %in% mcdout$best,TRUE,FALSE)
     mcdcov<-cov(mat[mcdset,])
     singlecov <- sum(diag(mcdcov))
     sqcov <- sum(diag(mcdcov)^2)
     return (c(singlecov,sqcov))
}

parCM.function <- function(dataSize) {
     library(MASS)
     library(DetMCD)
     out <- sapply(c(1:sims),cmest.function,size=dataSize)
     ave1=sum(out[1,])
     sq1=sum(out[2,])
     c1=ave1/(sims*dataDim)
     var1<-(sq1 - (c1*c1*sims*dataDim))/(sims*dataDim-1)
     m1<-2*c1*c1/var1
     return (c(dataDim,dataSize,m1,c1))
}

clus <- makeMPIcluster(10)
clusterExport(clus,'dataDim')
clusterExport(clus,'sims')
clusterExport(clus,'cmest.function')
out <- parSapply(clus,c(minSize:maxSize),parCM.function)
stopCluster(clus)
write.table(t(out),filename,row.names=FALSE,col.names=FALSE)
