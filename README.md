Arquivos:

- params.r: Arquivo que calcula os parametros da distribuição F
- runpredict.r: Arquivo que contém todo o tratamento dos dados, detecção de outliers e treinamento do modelo. Se for executado precisa indicar o path absoluto com a pasta dos dados (pasta 'data').
- missingdata.txt: Arquivo com anotações
- runparams.sh: Arquivo que executa o algoritmo de busca de parametros (params.r)
