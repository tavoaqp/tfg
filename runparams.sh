#!/bin/bash

rm output.log

R --no-save --args --parallel --cpus=8 --type="SOCK" --hosts=localhost:8 --tmpdir=/home/ubuntu --restoreSR 5 25 1000 100 < params.r > output.log

