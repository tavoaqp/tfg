library("mice")
library("glmnet")
library("dplyr")
library(DetMCD)
library(stats)

###############LOADING TRAINING DATA####################
##Reading training data
data=read.csv("clean_training_progress_predictor-3.csv",colClasses=c("character","numeric","numeric","Date",rep("numeric",8),"character","numeric","numeric"))
data=mutate(data,device=ifelse(data$device=="iphone",3,ifelse(data$device=="ipad",2,1)))
data$rating=as.numeric(data$rating+2)


##Discarding columns that have irrelevant or too much missing data
data=subset(data,select=-c(user,revenue,units))
dateData=subset(data,is.na(ls_date)==FALSE)
dateData$ls_date=ifelse(dateData$ls_date=="2015-05-25",1,ifelse(dateData$ls_date=="2015-05-26",2,3))

#ttp has value 0 for data without date, total_sessions make MCD not converge
noDateData=subset(data,is.na(ls_date)==TRUE,select=-c(ls_date,tsls,tbs,ttp,total_sessions))


###############COMPLETING MISSING DATA####################
##Running imputation model on missing values for data with a date
tempDateData=mice(subset(dateData,select=c(completed,completed_post,win_rate,tries,tbs)),m=10,maxit=50,meth='fastpmm',seed=500)
impDateData=complete(tempDateData,1)
dateData[,colnames(impDateData)]=impDateData

##Running imputation model on missing valyes for data without a date
tempNoDateData=mice(subset(noDateData,select=c(completed,completed_post,win_rate,tries)),m=10,maxit=50,meth='fastpmm',seed=500)
impNoDateData=complete(tempNoDateData,1)
noDateData[,colnames(impNoDateData)]=impNoDateData

###############LOADING TEST DATA####################

##Reading test data
testData=read.csv("clean_test_progress_predictor-4.csv",colClasses=c("character","numeric","numeric","Date",rep("numeric",7),"character","numeric","numeric"))
testData=mutate(testData,device=ifelse(testData$device=="iphone",3,ifelse(testData$device=="ipad",2,1)))
testData$rating=as.numeric(testData$rating+2)


##Discarding columns that have irrelevant or too much missing data
testData=subset(testData,select=-c(user,revenue,units))
testDateData=subset(testData,is.na(ls_date)==FALSE)
testDateData$ls_date=ifelse(testDateData$ls_date=="2015-05-25",1,ifelse(testDateData$ls_date=="2015-05-26",2,3))

#ttp has value 0 for data without date
testNoDateData=subset(testData,is.na(ls_date)==TRUE,select=-c(ls_date,tsls,tbs,ttp,total_sessions))

#indices useful to build up final output
testDateDataIdx=c(1:length(testData$device))[ifelse(is.na(testData$ls_date),FALSE,TRUE)]
testNoDateDataIdx=c(1:length(testData$device))[ifelse(is.na(testData$ls_date),TRUE,FALSE)]

###############COMPLETING MISSING DATA####################

##Running imputation model on missing values for data with a date
tempTestDateData=mice(subset(testDateData,select=c(completed,win_rate,tries,tbs)),m=10,maxit=50,meth='fastpmm',seed=500)
impTestDateData=complete(tempTestDateData,1)
testDateData[,colnames(impTestDateData)]=impTestDateData

##Running imputation model on missing valyes for data without a date
tempTestNoDateData=mice(subset(testNoDateData,select=c(completed,win_rate,tries)),m=10,maxit=50,meth='fastpmm',seed=500)
impTestNoDateData=complete(tempTestNoDateData,1)
testNoDateData[,colnames(impTestNoDateData)]=impTestNoDateData

###############PCA ANALYSIS####################

## PCA shows that almost all columns have a relevant contribution to data

#dateDataPca=prcomp(dateData,center=TRUE,scale.=TRUE)
#noDateDataPca=prcomp(noDateData,center=TRUE,scale.=TRUE)
#summary(dateDataPca)
#summary(noDateDataPca)

###############OUTLIER ANALYSIS####################

featureDateData=subset(dateData,select=-c(completed_post))
outputDateData=subset(dateData,select=c(completed_post))

featureNoDateData=subset(noDateData,select=-c(completed_post))
outputNoDateData=subset(noDateData,select=c(completed_post))

dropOutliers <- function(dataPar, m, c) {
             dataDim=length(dataPar)
             cutoff=qf(.975,df1=dataDim,df2=m-dataDim+1)
             mah1=DetMCD(dataPar)
             fValue=c*(m-dataDim+1)/(dataDim*m)
             fixedMah=fValue*mahalanobis(dataPar,mah1$raw.center,mah1$raw.cov)
             c(1:length(dataPar$device))[fixedMah<cutoff]
}

m1=2205.29467917113
c1=0.807013961540982
featureDateDataIdx=dropOutliers(featureDateData,m1,c1)

m2=913.489841027
c2=0.735901265643987
featureNoDateDataIdx=dropOutliers(featureNoDateData,m2,c2)

featureDateData=featureDateData[featureDateDataIdx,]
featureNoDateData=featureNoDateData[featureNoDateDataIdx,]

outputDateData=outputDateData[featureDateDataIdx,]
outputNoDateData=outputNoDateData[featureNoDateDataIdx,]

###############TRAINING LINEAR REGRESSION WITH k-FOLD CROSS VALIDATION####################

dateModel=cv.glmnet(as.matrix(featureDateData),as.matrix(outputDateData),family="gaussian",nfolds=50)

predictDateData=predict.cv.glmnet(dateModel,s=c("lambda.min"),newx=as.matrix(testDateData),type="response")

noDateModel=cv.glmnet(as.matrix(featureNoDateData),as.matrix(outputNoDateData),family="gaussian",nfolds=50)

predictNoDateData=predict.cv.glmnet(noDateModel,s=c("lambda.min"),newx=as.matrix(testNoDateData),type="response")

output=c(1:length(testData$device))
output[testDateDataIdx]=predictDateData
output[testNoDateDataIdx]=predictNoDateData

write.table(output,"predict_output.txt",row.names=TRUE,col.names=FALSE)
